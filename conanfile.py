from conans import ConanFile, CMake, tools
import os


class FixedpointConan(ConanFile):
	name = "fixed_point"
	version = "0.1"
	settings = "os", "compiler", "build_type", "arch"
	license = "Boost software license 1.0"
	url = "https://gitlab.com/paulbendixen/conanFixedPoint"
	generators = "cmake"
	description = """Experimental implementation of the suggested fixed_point type from the  P0037 paper"""

	def source(self):
		self.run("git clone https://github.com/johnmcfarlane/fixed_point.git")
		self.run("cd fixed_point && git checkout develop")
		# This small hack might be useful to guarantee proper /MT /MD linkage in MSVC
		# if the packaged project doesn't have variables to set it properly

	def build( self ):
		# Build package is only used for making shure the test succeeds, thanks to @drodri on the cpp slack for the idea
		#also changed to use cmake class as per his suggestion
		#Should probably find out how to set CMAKE_CXX_STANDARD 14
		cmake = CMake( self )
		cmake.configure( source_dir="fixed_point" )
		cmake.build()

	def package(self):
		 # All files must be copied, since there are no endings on the headerfiles
		self.copy("*", dst="include/sg14", src="fixed_point/include/sg14")
		self.copy("*", dst="include/sg14/auxiliary", src="fixed_point/include/sg14/auxiliary")
		self.copy("*", dst="include/sg14/bits", src="fixed_point/include/sg14/bits")

	def package_id( self ):
		self.info.header_only()
